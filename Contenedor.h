#include <string>
#include <list>
#include <iostream>
using namespace std;

class Contenedor{
	private:
		string empresa;
		int numero_s;
	public:
		Contenedor();
		Contenedor(string empresa, int numero_s);
		
		void set_empresa(string empresa);
		void set_numero_s(int numero_s);
		string get_empresa();
		int get_numero_s();
};
