prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Puerto.cpp Pilas.cpp Contenedor.cpp
OBJ = Puerto.o
APP = Puerto

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

