#include <string>
#include <iostream>
using namespace std;
#include "Pilas.h"

int tope;
bool band;
int max;


Pila::Pila(int tope, bool band, int max){
	this->tope = tope;
	this->band = band;
	this->max = max;
	this->pila = new Contenedor[this->max];
}

void Pila::go_tope(int tope){
	this->tope = tope;
} 
		
void Pila::set_tope(int tope, int max, int in_out){
		
	if (in_out == 1){
		if (tope == max){
			cout<<"~~~~~~~~~~~Pila llena~~~~~~~~~~~"<<endl;
		}
		if (tope < max){
			this->tope = tope + 1;
		}
	}
	if (in_out == 2){	
		if(tope == 0){
			cout<<"~~~~~~~Pila sin elementos~~~~~~~"<<endl;
		}			
		if (tope > 0){
			this->tope = tope - 1;
		}
	}
	
}
		
void Pila::set_band(int tope){
	if (tope == 0){
		this->band = true;
	}
	else{
		this->band = false;
	}
}
		
void Pila::set_max(int max){
	this->max = max;
}
		
void Pila::set_pila(int max){
	this->pila = new Contenedor[max];
}
		
int Pila::get_tope(){
	return this->tope;
}
int Pila::get_max(){
	return this->max;
}
		
void Pila::fill_pila(int tope, Contenedor numb){
	this->pila[tope].set_empresa(numb.get_empresa());
	this->pila[tope].set_numero_s(numb.get_numero_s());
	
}
		
Contenedor Pila::see_pila(int tope){
	return this->pila[tope];
}
		
bool Pila::get_band(){
	if (this->band == true){
		cout<<"~~~~~~~Pila sin elementos~~~~~~~"<<endl;
	}
	return this->band;
}
