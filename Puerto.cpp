#include <string>
#include <iostream>
using namespace std;
#include "Pilas.cpp"

void print_pila(Pila pila[], int tamano, int cantidad){

	for(int j = tamano-1; j >= 0; j--){
				
		for(int i = 0; i < cantidad; i++){
			cout<<"=";						
			cout<<"["<< pila[i].see_pila(j).get_empresa()<<"--->"<< pila[i].see_pila(j).get_numero_s<<"]";
			cout<<"=";	
		}
		cout<<endl;
	}
}	

int main(){
	//inicio se crea como "4" para que "parta" el programa//
	int inicio = 0, tamano, cantidad, coor_puer, coor_pil, if_there, temp_n = 000, in_n, out_n;
	string mar, num, in, out, temp = "------";
	Contenedor fill;
		
	
	fill.set_nombre(temp);
	fill.set_numero_s(temp_n);
	//se le pide el tamaño de la pila al usuario//
	cout << "Ingrese tamaño Máximo de las pilas: "<<endl;
	cout<<"~~~~~> ";
	getline(cin, num);
	tamano = stoi(num);	
	while(tamano <= 0){
		cout<<"Cantidad invalida, ingrese de nuevo"<<endl;
		cout<<"Tamaño: ";
		getline(cin, num);
		tamano = stoi(in);
	}
	
	//se le pide el cantidad de pilas al usuario//
	cout << "Ingrese cantidad Máxima de pilas: ";
	getline(cin, num);
	cantidad = stoi(num);	
	
	Pila pila = new Pila[cantidad];
	
	//se llenan los datos basicos de las pilas//
	for(int i = 0; i < cantidad; i++){
		pila[i].go_tope(0);
		pila[i].set_max(tamano);
		pila[i].set_pila(pila[i].get_max());
		for(int j = 0; j < pila[i].get_max(); j++){
			pila[i].fill_pila(j, fill);
		}
	}
	while (inicio != 0){
		
		//cada band debe renovarse en cada iteración//
		for(int i = 0; i < cantidad; i++){		
			pila[i].set_band(pila[i].get_tope());
		}
		if_there = 0;
	    
		//como primera accion se imprime el menú//	
		cout<<"Agregar contenedor        [1]"<<endl;
		cout<<"Remover contenedor        [2]"<<endl;	
		cout<<"Ver Puerto                [3]"<<endl;
		cout<<"Salir                     [4]"<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
		cout<<"Opción:  "<<endl;
		cout<<"~~~~~>";
		
		
		//se pide la opción//,
		getline(cin, mar);
		inicio = stoi(mar);
		
		
		switch(inicio){
			case 1:
				//si se escoge la opcion push se puede verificar el valor max vs tope// 
				for (int g=0; g<cantidad; g++){
					//se recorre el puerto buscando la primer pila con espacio
					if (pila[g].get_tope() < pila[g].get_max()){
						
						//si hay espacio se pide la empresa dueña del contenedor que tomara la nueva posicion tope//
						cout << "Ingrese nombre de la empresa: ";
						getline(cin, mar);
						in = mar;
						fill.set_empresa(in);
						//se pide el numero del contenedor que tomara la nueva posicion tope//
						cout << "Ingrese numero del contenedor: ";
						getline(cin, mar);
						in_n = stoi(mar);
						fill.set_numero_s(in_n);
						
						
						pila[g].set_tope(pila[g].get_tope(), pila[g].get_max(), inicio);
						//se añade el valor ingresado in a la pila//
						pila[g].fill_pila(pila[g].get_tope(), fill);
						break;
					}else{
						cout<<"la pila N°"<<(g+1)<<" está llena"<<endl;
					}
				}
				break;
			case 2:
				//se pegunta que contenedor desea utilizar//
				cout<<"que numero de contenedor desea extraer? ";
				getline(cin, out);
				out_n = stoi(out);
				//se recorre el "puerto" buscando al contenedor escogido
				for(int j = 0; j < cantidad; j++){
					for(int i = 0; i < pila[j].get_max(); i++){
						if(pila[j].see_pila(i).get_numero_s == out_n){
							//se guardan las "coordenadas" de el objeto//
							coor_puer = j;
							coor_pil = i;
							if_there = 1;	
							break;
						}
					}		
					if(if_there == 1){
						break;
					}		
				}
				//si el contenedor no se encuentra en el puerto se le avisa al usuario//
				if(if_there == 0){
					cout<<"El contenedor "<<out<<" no se encuentra"<<endl;
					break;
				}
				if(if_there == 1){
					if (pila[coor_puer].get_tope() == coor_pil){
						fill.set_nombre(temp);
						fill.set_numero_s(temp_n);
						pila[coor_puer].fill_pila(coor_pil, fill);
						coor_pil= coor_pil--;
						pila[coor_puer].go_tope(coor_pil);
					}else{
						
					}
					
				}
				
				//se recorre la pila que guarda el contenedor y se buscan los datos "no vacios"//
				for(int i = tamano-1; i > coor_pil; i--){
										
					//cuando un dato "no vacio" sea hallado, se busca la siguiente pila con espacios libres// 
					for(int j = coor_puer +1; j < cantidad; j++){
						//cuando se halla el ultimo dato de la pila objetivo se traspasa a la pila siguiente//
						if (pila[j].get_tope() < pila[j].get_max()-1){
								
							//se modifica su tope//
							pila[j].set_tope(pila[j].get_tope(), pila[j].get_max(), 1);		
							//se añade el valor objetivo-1 in a la pila//
							pila[j].fill_pila(pila[j].get_tope(), pila[coor_puer].see_pila(i));
							
							//se repone el valor retirado a "fill"//
							pila[coor_puer].fill_pila(pila[coor_puer].get_tope(), fill);
							//y se "elimina" su posicion//
							pila[coor_puer].set_tope(pila[coor_puer].get_tope(), pila[coor_puer].get_max(), inicio);			
							break;
						}
							
						if(j == cantidad-1){
							j = 0;
						}
					}	
				}
			
				
				
				//y se "elimina" su posicion//
				pila[coor_puer].set_tope(pila[coor_puer].get_tope(), pila[coor_puer].get_max(), inicio);
				break;
			case 3:
				//si se escoge imprimir pila se imprime un "titulo" primero// 
				cout<<"--------------------------------"<<endl;
				cout<<"--------------Pila--------------"<<endl;
				//luego se recorre la pila desde la posicion "tope" hasta 0"
				print_pila(pila, tamano, cantidad);
				cout<<"--------------------------------"<<endl;
				break;
			
			case 4: 
				cout<<"---Gracias por utilizar este programa---"<<endl;
				cout<<"----------tenga un buen día-------------"<<endl;
				break;
			
			default: cout<< "---------Ingreso invalido-------"<<endl; break;
		}
		
	}
			
}
